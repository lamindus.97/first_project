<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateAdminCommand extends Command
{
    protected static $defaultName = 'app:create-admin';
    private $requirePassword;
    private $em;
    public function __construct(UserPasswordHasherInterface $requirePassword, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->requirePassword = $requirePassword;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'The email of the admin')
            ->addArgument('password', $this->requirePassword ? InputArgument::REQUIRED : InputArgument::OPTIONAL, 'admin password')
            ->setDescription('Creates a new admin')
            ->setHelp('This command allows you to create a user-admin...');
    }
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $admin = new User();

        $admin->setEmail($input->getArgument('email'));
        $password = $this->requirePassword->hashPassword($admin, $input->getArgument('password'));
        $admin->setPassword($password);
        $admin->setRoles(['ROLE_ADMIN']);

        $this->em->persist($admin);
        $this->em->flush();

        $output->writeln([
            'Admin Creator',
            '------------',
            '',
        ]);

        $output->writeln('Email: '.$input->getArgument('email'));
        $output->writeln('');

        $output->writeln('You are about to create the admin.');
        return Command::SUCCESS;
    }
}