<?php

namespace App\Form;

use App\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $options['data'] ?? null;
        $isEdit = $entity && $entity->getId();
        $builder
            ->add('header', TextType::class, [
                'attr' => [
                    'placeholder' => 'Default header'
                ]
            ])
            ->add('briefDescription', TextType::class, [
                'attr' => [
                    'placeholder' => 'Default description'
                ]
            ])
            ->add('image', FileType::class, [
                'required' => !$isEdit,
                'mapped' => false, // unmapped means that this field is not associated to any entity property
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}
