<?php


namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends AppFixtures
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->encoder = $passwordEncoder;
    }
    public function load(ObjectManager $manager)
    {
        for($i = 2; $i < 3; $i++) {
            $user = new User();
            $user->setEmail('admin'.$i.'@test.ua');

            $password = $this->encoder->encodePassword($user, 'pass_1234');
            $user->setPassword($password);
            $user->setRoles(["ROLE_ADMIN"]);

            $manager->persist($user);
            $manager->flush();
        }
    }
}