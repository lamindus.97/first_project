<?php

namespace App\Service;


use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class UploadService
{
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }
    public function imageUpload(UploadedFile $file, string $path): string
    {
        $originFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFileName = $this->slugger->slug($originFileName);
        $fileName = $path.'/'.$safeFileName.'-'.uniqid().'.'.$file->guessExtension();

        $file->move($path, $fileName);

        return $fileName;
    }
}
