<?php


namespace App\Entity;

use App\Repository\StatisticsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatisticsRepository::class)
 */
class Statistics
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length="32")
     */
    private $nameOfElement;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getNameOfElement(): ?string
    {
        return $this->nameOfElement;
    }
    public function setNameOfElement(string $nameOfElement): self
    {
        $this->nameOfElement = $nameOfElement;
        return $this;
    }
    public function getAmount() : ?int
    {
        return $this->amount;
    }
    public function setAmount(int $amount): self
    {
        $this->amount = $amount;
        return $this;
    }
}