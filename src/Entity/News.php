<?php


namespace App\Entity;

use App\Repository\NewsRepository;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass=NewsRepository::class)
 */
class News
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length="32")
     */
    private $header;

    /**
     * @ORM\Column(type="string", length="255")
     */
    private $briefDescription;

    /**
     * @ORM\Column(type="string", length="255")
     */
    private $image;

    public function getId() : ?int
    {
        return $this->id;
    }
    public function getHeader() : ?string
    {
        return $this->header;
    }
    public function setHeader(string $header): self
    {
        $this->header = $header;
        return $this;
    }
    public function getBriefDescription(): ?string
    {
        return $this->briefDescription;
    }
    public function setBriefDescription(string $briefDescription): self
    {
        $this->briefDescription = $briefDescription;
        return $this;
    }
    public function getImage(): string
    {
        return $this->image;
    }
    public function setImage($fileName): self
    {
        $this->image = $fileName;

        return $this;
    }
}