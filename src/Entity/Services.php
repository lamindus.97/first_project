<?php

namespace App\Entity;

use App\Repository\ServicesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ServicesRepository::class)
 */
class Services
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $nameOfService;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descriptionOfService;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameOfService(): ?string
    {
        return $this->nameOfService;
    }

    public function setNameOfService(string $nameOfService): self
    {
        $this->nameOfService = $nameOfService;

        return $this;
    }

    public function getDescriptionOfService(): ?string
    {
        return $this->descriptionOfService;
    }

    public function setDescriptionOfService(string $descriptionOfService): self
    {
        $this->descriptionOfService = $descriptionOfService;

        return $this;
    }
}

